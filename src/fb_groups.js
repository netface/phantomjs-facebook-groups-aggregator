/*!
 * Part of PhantomJS Facebook groups aggregator
 * Released under the MIT license
 * Author: Dominik Smaga <d.smaga@netface.pl>
 * 
 * Please read README.md file for details
 * 
 * Proceed with Facebbok HTML and save offers
 * 
 */

var fbGroups = {
  
  //main page object
  page : null, 
  
  //config keywords transformed do RegExps array
  matches : null, 
  
  //offers
  offers : {}, 
  
  /**
   * Login into Facebook - fill login form
   * 
   * @param {string} login
   * @param {string} password
   * @returns {void}
   */
  login : function ( login, password ) {
    
      fbGroups.page.evaluate( function ( login, password ) {

        var frm = document.getElementById("login_form");
        frm.elements["email"].value = login;
        frm.elements["pass"].value = password;
        frm.submit();

      }, login , password );  
      
  },
  
  /**
   * Scrap Facebook HTML and 
   * 
   * @returns {
   *    "nextUrl" : {string},
   *    "offers"  : {Array}
   * }
   */
  loadContent : function () {
    
    var data = fbGroups.page.evaluate( function() {
      
      var i, j, maxJ, k, anchors, match, nextUrl, len,
        header = null,
        articles = document.getElementsByClassName( "story_body_container" ),
        max = articles.length,
        offers = new Array(),
        re = [
          /(\/groups\/[a-z0-9\.]+\?view=permalink&id=(\d+))/i,
          /(story_fbid=(\d+))/i,
          /(mf_story_key\.(\d+))/i
        ],
        maxK = re.length;
        
      for ( i = 0; i < max; i++ ) {        
        
        anchors = articles[i].getElementsByTagName('a');
        maxJ = anchors.length;
        
        loop : 
          for ( j = 0; j < maxJ; j++ ) {
            for ( k = 0 ; k < maxK; k++ ) {
              match = anchors[j].getAttribute("href").match( re[k] );
              if ( match ) {
                break loop;
              } 
            }          
          }
        
        if(!match) {
          console.log(articles[i].innerHTML);
        }
        
        if (articles[i].getElementsByClassName("_4gus").length > 0) {
          articles[i].getElementsByClassName("_4gus")[0]
                  .removeChild(articles[i].getElementsByClassName("_4gus")[0]
                                       .getElementsByTagName('span')[0]);
          header = articles[i].getElementsByClassName("_4gus")[0].textContent || 
                      articles[i].getElementsByClassName("_4gus")[0].innerText;
        } else if ((len = articles[i].getElementsByTagName('h3').length) && (len > 0)) {
          header = articles[i].getElementsByTagName('h3')[len - 1].textContent || 
                      articles[i].getElementsByTagName('h3')[len - 1].innerText;
        }
        
        offers.push({
          "html" : articles[i].innerHTML,
          "text" : articles[i].textContent || articles[i].innerText,
          "id"   : match ? match[2] : null,
          "header" : header
        });        
      }
      
      try {        
        nextUrl = document.getElementById("m_more_item")
                          .getElementsByTagName('a')[0].getAttribute("href");                      
      } catch (error) {        
        nextUrl = null;        
      }
      
      return {
        "nextUrl" : nextUrl,
        "offers" : offers
      };
            
    } );
    
    return data;
    
  },
  
  saveOffers : function ( baseUrl ) {
    
    var data = fbGroups.loadContent();
    
    if (data.offers && data.offers.length > 0) {
      data.offers.filter( fbGroups.match )
               .forEach( fbGroups.saveOffer );
    }
    
    return data.nextUrl ? (baseUrl + data.nextUrl) : false;
    
  },
  
  saveOffer : function( offer ) {
    var id = ("0000000000" + offer.id).slice(-20);
    fbGroups.offers[id] = offer;
    console.log(Object.keys(fbGroups.offers).length + " offers");
    
  },
  
  getOffers : function () {
    
    var keys = Object.keys(fbGroups.offers).sort();
    
    var arr = [];
    
    for (var i = keys.length - 1; i >= 0; i-- ) {
      arr.push(fbGroups.offers[keys[i]]);
    }
    
    return arr;
    
  },
  
  scrollDown : function() {
    
    fbGroups.page.evaluate( function() {

      document.getElementById("m_more_item").getElementsByTagName('a')[0].click();
      
    } );
    
  },
  
  _setMatches : function ( matches, depth ) {
    
    var i, len = matches.length;
    
    for ( i = 0 ; i < len ; i++ ) {
      
      if ( Array.isArray(matches[i]) ) {
        
        matches[i] = fbGroups._setMatches( matches[i], 1 );
        
      } else if ( ( typeof matches[i] ) === "string" ) {
        
        matches[i] = depth ? new RegExp( matches[i], "i" ) : [ new RegExp( matches[i], "i" ) ];
        
      } else if ( matches[i] instanceof RegExp ) {
        
        matches[i] = depth ? matches[i] : [ matches[i] ];
        
      } else {
        
        throw "Wrong config at " 
            + i 
            + " position (" 
            + ( typeof matches[i] )  
            + ").";
        
      }
      
    }
    
    return matches;
    
  },
  
  setMatches : function ( matches ) {
    
    fbGroups.matches = fbGroups._setMatches( matches, 0 );
    
  },
  
  match : function ( content ) {
    
    var i, j, lenI = fbGroups.matches.length , lenJ, result;
    
    content = typeof content === "string" ? content : content.text ;    
    
    for ( i = 0 ; i < lenI ; i++ ) {
      
      result = true;
      
      lenJ = fbGroups.matches[i].length;
      
      for ( j = 0 ; j < lenJ ; j++ ) {
        
        result = result && fbGroups.matches[i][j].test( content );
        
      }
      
      if ( result ) {
        
        return true;
        
      }
      
    }
    
    return false;
    
  }
  
};

module.exports = fbGroups;


