/*!
 * Part of PhantomJS Facebook groups aggregator
 * Released under the MIT license
 * Author: Dominik Smaga <d.smaga@netface.pl>
 * 
 * Please read README.md file for details
 * 
 * Config file
 */

module.exports = {
  
  /* Your Facebook login */
  login : "Your Facebook login", 
  
  /* Your Facebook password */
  password : "Your Facebook password",
  
  /* Groups to agregate - links starts with https://m.facebook.com/groups/ */  
  groups : [
    "https://m.facebook.com/groups/programistaposzukiwany"    
  ],
  
  /* Keywords to search 
   * 
   * Example below means: 
   * Find all posts from above groups
   * contains (case insensitive)
   * ('php' AND 'remote') OR ('javascript' AND 'senior')
   */
  keywords : [
    [ 
      /php/ig, 
      /remote/ig
    ],
    [
      /javascript/ig,
      /senior/ig
    ]
  ],
  
  /* In mobile FB there is 7 post/offers for each page */
  pages : 5,
  
  /* Restart FB scan time */
  refresh: 300000,
  
  /* Browser auto-refresh time */
  browserRefresh: 30000,
  
  /* Server address */
  serverAddr : '127.0.0.1:8088',
  
  /* Base url - leave it as is */
  baseUrl : "https://m.facebook.com"
  
};


