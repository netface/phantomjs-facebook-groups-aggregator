/*!
 * Part of PhantomJS Facebook groups aggregator
 * Released under the MIT license
 * Author: Dominik Smaga <d.smaga@netface.pl>
 * 
 * Please read README.md file for details
 * 
 * Server module
 */

var webserver = require('webserver');

module.exports = {
  server: function (config, fbGroups, WebPage) {

    webserver.create().listen(config.serverAddr, function (request, response) {
      
      var match = request.url.match(/\/id\/(\d+)/);
      
      response.statusCode = 200;
      
      if (match) { // url like /id/fb_story_id

        // Becouse Facebook didn't allow display their posts inside the iframe
        // we have to create simple proxy to download and display Facebook
        // content
        
        var p = new WebPage();
        
        p.loadImages = false;
        p.settings.userAgent = "Opera/9.80 (J2ME/MIDP; " +
            "Opera Mini/6.5.26955/27.1407; U; en) Presto/2.8.119 Version/11.10";
        
        p.open(config.baseUrl + '/' + match[1], function (status) {
          
          // We do some hacks: hidding unneeded content and adding base href
          var content = p.content.replace('<title>', '<base href="'
              + config.baseUrl
              + '" target="_blank">'
              + '<style>'
              + '#header,#m_story_permalink_view>div:last-child,#root>div,'
              + '#viewport>div'
              + '{display:none !important;}'
              + '#root>div:first-child,#viewport>#objects_container'
              + '{display:block !important;}'
              + '</style><title>');
          response.write(content);
          response.close();
          
        });

      } else {

        // Main page served by phantomJS server with simple offer list.
        // We use simple Bootstrap view without not-required content.
        // You can use keyboard to control <Enter> to open first offer
        // in modal window, <Left>, <Right> to display prev/next item.
        response.write('<!doctype html>\n' +
            '<html lang="en">' +
            '<head>' +
            '<title>Phantom FB Groups</title>' +
            '<meta charset="utf-8">' +
            '<meta name="viewport" content="width=device-width, ' +
            'initial-scale=1, shrink-to-fit=no">' +
            '<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/' +
            'bootstrap/4.0.0-beta.2/css/bootstrap.min.css" ' +
            'integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrs' +
            'zuE4W1povHYgTpBfshb" crossorigin="anonymous">' +
            '<style>' +
            'a:visited {color: grey;}' +
            '.modal-title {' +
            'overflow: hidden;' +
            'text-overflow: ellipsis;' +
            'display: -webkit-box;' +
            '-webkit-box-orient: vertical;' +
            '-webkit-line-clamp: 1;' +
            'max-height: 1.5em;' +
            '}' +
            '</style>' +
            '</head>' +
            '<body>' +
            '<div class="container">');

        fbGroups.getOffers().forEach(function (item) {

          response.write('<li><a href="' +
              "/id/" + item.id +
              '" target="_blank" ' +
              ' data-toggle="modal" ' +
              ' data-target="#exampleModal" ' +
              ' class="offer-link" ' +
              '>' + (item.header || item.text) + '</a></li>');

        });

        response.write('</div>' +
            //Modal
            '<div class="modal fade" id="exampleModal" tabindex="-1" ' +
            'role="dialog" aria-labelledby="exampleModalLabel" ' +
            'aria-hidden="true">' +
            '<div class="modal-dialog" role="document">' +
            '<div class="modal-content">' +
            '<div class="modal-header">' +
            '<h5 class="modal-title" id="exampleModalLabel"></h5>' +
            '<button type="button" class="close" data-dismiss="modal" ' +
            'aria-label="Close">' +
            '<span aria-hidden="true">&times;</span>' +
            '</button>' +
            '</div>' +
            '<div class="modal-body">' +
            '<iframe width="100%" height="400" frameborder="0" ' +
            'allowfullscreen=""></iframe>' +
            '</div>' +
            '<div class="modal-footer">' +
            '<button type="button" class="btn btn-primary btn-prev">' +
            'Prev</button>' +
            '<button type="button" class="btn btn-primary btn-next">' +
            'Next</button>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div>' +
            // Frontend JavaScript libs
            '<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" ' +
            'integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDM' +
            'VNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>' +
            '<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/' +
            '1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7y' +
            'o7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" ' +
            'crossorigin="anonymous"></script>' +
            '<script src="https://maxcdn.bootstrapcdn.com/bootstrap/' +
            '4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-' +
            'alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ' +
            '" crossorigin="anonymous"></script>' +
            // Local scripts
            '<script>' +
            // Show modal content
            "var showModal = function(button, modal) {" +
            "var title = button.text();" +
            "var src = button.attr('href');" +
            "var current_url = window.location.href;" +
            "history.replaceState({},\"\",src);" +
            "history.replaceState({},\"\",current_url);" +
            "setVisited(src);" +
            "button.css('font-weight', 'normal');" +
            "modal.find('.modal-title').text(title);" +
            "modal.find('iframe').attr('src', src);" +
            "modal.find('.btn-prev').unbind('click').click(" +
            "function(){" +
            "if(button.parent().prev().length > 0){" +
            "showModal(button.parent().prev().find('a'), modal);" +
            "}" +
            "}" +
            ");" +
            "modal.find('.btn-next').unbind('click').click(" +
            "function(){" +
            "if(button.parent().next().length > 0){" +
            "showModal(button.parent().next().find('a'), modal);" +
            "}" +
            "}" +
            ");" +
            "};" +
            // On modal show event
            "$('#exampleModal').on('show.bs.modal', function (event) {" +
            "var button = $(event.relatedTarget);" +
            "var modal = $(this);" +
            "showModal(button, modal);" +
            "});" +
            // Refresh page when is non-active
            'var timeoutId = null;' +
            'var vis = (function(){' +
            'var stateKey, eventKey, keys = {' +
            'hidden: "visibilitychange",' +
            'webkitHidden: "webkitvisibilitychange",' +
            'mozHidden: "mozvisibilitychange",' +
            'msHidden: "msvisibilitychange"' +
            '};' +
            'for (stateKey in keys) {' +
            'if (stateKey in document) {' +
            'eventKey = keys[stateKey];' +
            'break;' +
            '}' +
            '}' +
            'return function(c) {' +
            'if (c) document.addEventListener(eventKey, c);' +
            'return !document[stateKey];' +
            '}' +
            '})();' +
            'vis(function(){' +
            'if (!vis()) {' +
            'timeoutId=setTimeout(function(){ location.reload(); }, ' +
            config.browserRefresh +
            ');' +
            '} else if (timeoutId !== null) {' +
            'clearTimeout(timeoutId);' +
            '}' +
            '});' +
            // Keyboard keys control
            'document.onkeydown = function(e){' +
            'e = e || window.event;' +
            'if (e.keyCode == "37") {' +
            '$(".btn-prev:visible").click();' +
            '}' +
            'else if (e.keyCode == "39") {' +
            '$(".btn-next:visible").click();' +
            '}' +
            'else if (e.keyCode == "13") {' +
            '$("a:first").click();' +
            '}' +
            '};' +
            // Visited
            'function setVisited(link){'+
            'localStorage.setItem("visited:" + link, true);' +
            '};' +
            'function isVisited(link){' +
            'return localStorage.getItem("visited:" + link);' +
            '};' +
            'function countNotVisited(){' +
            'var items = document.getElementsByClassName("offer-link");' +
            'var cnt = 0;' +
            'for (var i = items.length - 1; i >= 0; i--) {' +
            'if (!isVisited(items[i].getAttribute("href"))) {' +
            'items[i].style.fontWeight = "bold";' +
            'cnt++;' +
            '}' +
            '}' +
            'return cnt;' +
            '};' +
            'setTimeout(function(){' +
            'var cnt = countNotVisited();' +
            'if (cnt > 0) {' +
            'document.title = "New offers: " + cnt;' +
            '}' +
            '}, 2000);' +
            '</script>' +
            '</body>' +
            '</html>');

        response.close();

      }

    });
  }
};


