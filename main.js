/*!
 * PhantomJS Facebook groups aggregator
 * Released under the MIT license
 * Author: Dominik Smaga <d.smaga@netface.pl>
 * 
 * Please read README.md file for details
 */

/* global phantom, WebPage */

var config = require('./src/config.js'),
    fbGroups = require('./src/fb_groups.js'),
    server = require('./src/server.js'),
    page = new WebPage();

page.loadImages = false;
page.settings.userAgent = "Opera/9.80 (J2ME/MIDP; Opera Mini/6.5.26955/27.140" +
    "7; U; en) Presto/2.8.119 Version/11.10";

fbGroups.page = page;
fbGroups.setMatches(config.keywords);

// Main logic - load group webpage, restart scanning
var main = function () {

  var groupUrl;

  if (phantom.state === "logged-in") {

    // Initialize config
    phantom.groups = config.groups.slice();
    phantom.state = "load-group";
  
  } 
  
  if (phantom.state === "load-group") {

    // Load group url from config copy
    // restart scanning if not exists
    if (!(groupUrl = phantom.groups.pop())) {

      setTimeout(function () {

        phantom.state = "logged-in";
        main();

      }, config.refresh);

      return;
    }

    // Open group webpage
    console.log("Load: " + groupUrl);
    page.open(groupUrl);
    phantom.paginated = 0;

  }

};

// PhantomJS events
page.onLoadFinished = function (status) {
  
  if (status === "success") {
    
    // Just after login run main()
    if (phantom.state === "logged-in") {
      return main();
    }
    
    // Scrap FB HTML
    var nextPage = fbGroups.saveOffers(config.baseUrl);
    
    // Goto next group if nedded
    if (!nextPage || phantom.paginated >= config.pages) {
      phantom.state = "load-group";
      return main();
    }
    
    // Load next group page
    console.log("Load: " + nextPage);
    page.open(nextPage);
    phantom.paginated++;
  }
  
};

page.onConsoleMessage = function (message) {

  console.log("msg: " + message);

};

// Login into Facebook and proceed
console.log("Load: " + config.baseUrl);
page.open(config.baseUrl, function (status) {
  if (status === "success") {
    fbGroups.login(config.login, config.password);
    phantom.state = "logged-in";    
  }
});

// Run server
server.server(config, fbGroups, WebPage);