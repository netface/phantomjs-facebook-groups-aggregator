# PhantomJS Facebook groups aggregator

## Preamble

To run this script you need to have PhantomJS, please
go to [http://phantomjs.org/](http://phantomjs.org/) and download it at first.

This script is tested on Linux OS only. 
There is no performance optimization.

As of Faceboook`s Terms of Service [https://www.facebook.com/terms.php](https://www.facebook.com/terms.php) 
propably you need their prior permission to use this script :) 
Read Facebook ToS!!!

## About

This script loads posts/offers from your Facebook groups, filters them and displays
in browser by built-in web server. It allows to have most recent and most interesting 
offers/posts from all yours groups in one place.
 

## Install and run

```bash
# Clone repository or download it and extract
$ git clone git@bitbucket.org:netface/phantomjs-facebook-groups-aggregator.git

# Go to script directory
$ cd phantomjs-facebook-groups-aggregator

# Create default config file
$ cp src/config.dist.js src/config.js

# Edit your own config file and fill your login and password
# also create your own filters
$ vi src/config.js

# Run
$ phantomjs main.js
```

Open browser and go to link: [http://127.0.0.1:8088](http://127.0.0.1:8088) 
or use your own addres, if it was changed in config file.